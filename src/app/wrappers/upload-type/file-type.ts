import { Component } from "@angular/core";
import { FieldGroupTypeConfig, FieldType, FieldTypeConfig } from "@ngx-formly/core";
import { MessageService } from "primeng/api";
import { UploadEvent } from "primeng/fileupload";

@Component({
    selector: 'formly-file-upload',
    template: `<div>
        <p-toast></p-toast>
    <p-fileUpload mode="basic" chooseLabel="Choose" maxFileSize="1000000" ></p-fileUpload>
    </div>`
})

export class FileType extends FieldType<FieldTypeConfig> {



}

import { Component } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';

import { of } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  reset() {
    this.form.reset();
  }

  form = new FormGroup({});
  model: any = {};
  options: FormlyFormOptions = {};

  // Simple design

  fields: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'p-fluid p-formgrid p-grid',
      wrappers: ['panel'],
      templateOptions: { label: 'Registration Form' },
      fieldGroup: [
        {
          key: 'firstName',
          type: 'input',

          templateOptions: {
            label: 'First Name',
            required: true,
          },
          className: 'p-field p-col-12 p-md-6',
        },
        {
          template: `<div class="mt-2"></div>`
        },
        {
          key: 'lastName',
          type: 'input',
          props: {
            label: 'Last Name',
            required: true,
          },

          className: 'p-field p-col-12 p-md-6',
        },
        {
          template: `<div class="mt-2"></div>`
        },
        {
          key: 'email',
          type: 'input',
          templateOptions: {
            label: 'Email',
            required: true,
            type: 'email',
          },
          validators: {
            email: {
              expression: (c: AbstractControl) => {
                if (!c.value) {
                  return true;
                }
                // Email validation regular expression
                const emailRegex =
                  /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
                return emailRegex.test(c.value);
              },
              message: 'Please enter a valid email address',
            },
          }, className: 'p-field p-col-12 p-md-6',
        },
        {
          template: `<div class="mt-2"></div>`
        },
        {
          key: 'password',
          type: 'input',
          templateOptions: {
            label: 'Password',
            required: true,
            type: 'password',
            minLength: 6,
          },
          validators: {
            uppercase: {
              expression: (c: AbstractControl) => !c.value || /[A-Z]/.test(c.value),
              message: 'Password should have at least 1 uppercase letter',
            },
            lowercase: {
              expression: (c: AbstractControl) => !c.value || /[a-z]/.test(c.value),
              message: 'Password should have at least 1 lowercase letter',
            },
            number: {
              expression: (c: AbstractControl) => !c.value || /\d/.test(c.value),
              message: 'Password should have at least 1 number',
            },
          },
          className: 'p-field p-col-12 p-md-6',
        },
        {
          template: `<div class="mt-2"></div>`
        },
        {
          key: 'retypePassword',
          type: 'input',
          templateOptions: {
            label: 'Retype Password',
            required: true,
            type: 'password',
          },
          validators: {
            fieldMatch: {
              expression: (control: any) => control.value === this.model.password,
              message: 'Passwords do not match',
            },
            uppercase: {
              expression: (c: AbstractControl) => !c.value || /[A-Z]/.test(c.value),
              message: 'Password should have at least 1 uppercase letter',
            },
            lowercase: {
              expression: (c: AbstractControl) => !c.value || /[a-z]/.test(c.value),
              message: 'Password should have at least 1 lowercase letter',
            },
            number: {
              expression: (c: AbstractControl) => !c.value || /\d/.test(c.value),
              message: 'Password should have at least 1 number',
            },

          },
          className: 'p-field p-col-12 p-md-6',
        },
        {
          template: `<div class="mt-2"></div>`
        },
        {
          key: 'dob',
          type: 'datepicker',
          templateOptions: {
            label: 'Date of Birth',
            placeholder: 'Date of Birth',
            numberOfMonths: 1,
            showIcon: true,
            required: true
          },
          validators: {
            dateOfBirth: {
              expression: (control: AbstractControl) => {
                const selectedDate = control.value as Date;
                const today = new Date();
                today.setHours(0, 0, 0, 0); // Set hours to 0 to compare dates without time
                return selectedDate <= today;
              },
              message: 'Date of birth cannot be a future date',
            },
            dob: {
              expression: (control: any) => {
                const dob = new Date(control.value);
                const ageDiffMs = Date.now() - dob.getTime();
                const ageDate = new Date(ageDiffMs);
                const age = Math.abs(ageDate.getUTCFullYear() - 1970);
                return age >= 18;
              },
              message: 'Age should be at least 18',
            },
          },
          className: 'p-field p-col-12 p-md-6',
        },
        {
          template: `<div class="mt-2"></div>`
        },
        {
          key: 'gender',
          type: 'radio',
          className: 'p-field p-col-12 p-md-6',
          defaultValue: 'Female',
          templateOptions: {
            label: 'Gender',
            required: true,
            options: [
              {
                label: 'Male',
                value: 'Male',
              },
              {
                label: 'Female',
                value: 'Female'
              },
            ],
          },
        },

        {
          template: `<div class="mt-2"></div>`
        },
        {
          key: 'interest',
          templateOptions: {
            label: 'Interest',
          },
          wrappers: ['form-field'],
          // expressionProperties: {
          //   'props.required': (model: any) => model.gender === 'Male',
          // },
          fieldGroup: [
            {
              key: 'cricket',
              type: 'checkbox',
              props: {
                label: 'Cricket',
                value: 'Cricket',
              },
            },
            { template: '<div class="mt-2"></div>' },
            {
              key: 'music',
              type: 'checkbox',
              props: {
                label: 'Music',
                value: 'music',
              },
            },
            { template: '<div class="mt-2"></div>' },
            {
              key: 'traveling',
              type: 'checkbox',
              props: {
                label: 'Traveling',
                value: 'traveling',
              },
            },
            { template: '<div class="mt-2"></div>' },
            {
              key: 'news',
              type: 'checkbox',
              props: {
                label: 'Reading Newspaper',
                value: 'news',
              },
            }
          ],
        }
      ],
    },
    { template: '<div class="mt-2"></div>' },
    {
      key: 'address',
      hide: true,
      wrappers: ['panel'],
      templateOptions: { label: 'Address' },
      fieldGroup: [
        {
          type: 'repeat',
          key: 'addresses',
          templateOptions: {},
          fieldArray: {
            wrappers: ['panel'],
            fieldGroupClassName: 'p-fluid',
            fieldGroup: [
              {
                type: 'repeat-delete',
                templateOptions: {
                  label: 'Remove',
                },
              },
              {
                key: 'town',
                type: 'input',
                templateOptions: {
                  required: true,
                  type: 'text',
                  label: 'Town',
                },
                className: 'p-col-4',
              },
              {
                key: 'state',
                type: 'input',
                templateOptions: {
                  required: true,
                  type: 'text',
                  label: 'State',
                },
                className: 'p-col-4',
              },
              {
                key: 'country',
                type: 'input',
                templateOptions: {
                  required: true,
                  type: 'text',
                  label: 'Country',
                },
                className: 'p-col-4',
              },
            ],
          },
        },
      ],
    },
  ];

  A: FormlyFieldConfig[] = [
    {
      key: 'gender',
      type: 'multi-radio',
      className: 'col-3',
      defaultValue: 'Female',
      templateOptions: {
        label: 'Gender',
        required: true,
        options: of([
          {
            value: 'Male',
            label: 'Male'
          },
          {
            value: 'Female',
            label: 'Female'
          }
        ]),
      },
    },
    {
      key: 'interest',
      type: 'multi-checkbox',
      // templateOptions: {
      //   label: 'Interest',
      //   required: true,
      //   options: [
      //     { value: 'Accounting', label: 'Accounting' },
      //     { value: 'Marketing', label: 'Marketing' },
      //     { value: 'Production', label: 'Production' },
      //     { value: 'Research', label: 'Research' },
      //   ],
      // },
      expressionProperties: {
        'templateOptions.required': 'model.gender === "Male"',
      },
    },
  ];

  // Using flex and row/columns
  fieldsS: FormlyFieldConfig[] = [
    {
      fieldGroupClassName: 'field grid p-fluid',
      fieldGroup: [
        {
          className: 'field col',
          type: 'input',
          key: 'firstName',
          props: {
            label: 'First Name',
            required: true
          },
        },
        {
          className: 'field col w-full',
          type: 'input',
          key: 'lastName',
          props: {
            label: 'Last Name',
            required: true
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'field grid p-fluid',
      fieldGroup: [
        {
          className: 'field col',
          type: 'input',
          key: 'email',
          props: {
            label: 'Email',
            required: true
          },
          validators: {
            email: {
              expression: (c: AbstractControl) => {
                if (!c.value) {
                  return true;
                }
                // Email validation regular expression
                const emailRegex =
                  /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
                return emailRegex.test(c.value);
              },
              message: 'Please enter a valid email address',
            },
          },
        },
        {
          className: 'field col',
          type: 'datepicker',
          key: 'dob',
          props: {
            label: 'Date of Birth',
            numberOfMonths: 1,
            required: true,
            showIcon: true
          },
          validators: {
            dateOfBirth: {
              expression: (control: AbstractControl) => {
                const selectedDate = control.value as Date;
                const today = new Date();
                today.setHours(0, 0, 0, 0); // Set hours to 0 to compare dates without time
                return selectedDate <= today;
              },
              message: 'Date of birth cannot be a future date',
            },
            dob: {
              expression: (control: any) => {
                const dob = new Date(control.value);
                const ageDiffMs = Date.now() - dob.getTime();
                const ageDate = new Date(ageDiffMs);
                const age = Math.abs(ageDate.getUTCFullYear() - 1970);
                return age >= 18;
              },
              message: 'Age should be at least 18',
            },
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'field grid p-fluid',
      fieldGroup: [
        {
          className: 'field col',
          type: 'input',
          key: 'password',
          props: {
            label: 'Password',
            required: true,
            type: 'password',
            minLength: 6
          },
          validators: {
            uppercase: {
              expression: (c: AbstractControl) => !c.value || /[A-Z]/.test(c.value),
              message: 'Password should have at least 1 uppercase letter',
            },
            lowercase: {
              expression: (c: AbstractControl) => !c.value || /[a-z]/.test(c.value),
              message: 'Password should have at least 1 lowercase letter',
            },
            number: {
              expression: (c: AbstractControl) => !c.value || /\d/.test(c.value),
              message: 'Password should have at least 1 number',
            },
          },
        },
        {
          className: 'field col',
          type: 'input',
          key: 'retypePassword',
          props: {
            label: 'Re-type Password',
            required: true,
            type: 'password',
          },
          validators: {
            fieldMatch: {
              expression: (control: any) => control.value === this.model.password,
              message: 'Passwords do not match',
            }
          },
        },
      ],
    },
    {
      fieldGroupClassName: 'field grid p-fluid',
      fieldGroup: [
        {
          className: 'field col',
          type: 'radio',
          key: 'gender',
          defaultValue: 'Female',
          props: {
            label: 'Gender',
            required: true,
            options: [
              {
                label: 'Female',
                value: 'Female'
              },
              {
                label: 'Male',
                value: 'Male'
              }
            ]
          },
        },
        {
          className: 'field col',
          key: 'interest',
          type: 'multi-checkbox',
          templateOptions: {
            label: 'Interest',
            required: true,
            options: [
              {
                label: 'Cricket',
                value: 'cricket'
              },
              {
                label: 'Music',
                value: 'music'
              },
              {
                label: 'Movies',
                value: 'movies'
              },
              {
                label:'Traveling',
                value:'traveling'
              }
            ]

          },
          expressionProperties: {
            'templateOptions.required': 'model.gender === "Male"',
          },
          validation: {
            messages: {
              required: 'Interest is required'
            }
          }
        },
      ],
    },
    // {
    //   key: 'Upload',
    //   type: 'file',
    //   props: {
    //     label: 'Upload',
    //     required: true
    //   }
    // },
    {
      className: 'section-label',
      template: '<hr /><div></div>',
    },
    {
      key: 'address',
      // hide: true,
      wrappers: ['panel'],
      templateOptions: { label: 'Address' },
      fieldGroup: [
        {
          type: 'repeat',
          key: 'addresses',
          templateOptions: {},
          fieldArray: {
            wrappers: ['panel'],
            fieldGroupClassName: 'field grid p-fluid',
            fieldGroup: [
              {
                key: 'town',
                type: 'input',
                templateOptions: {
                  required: true,
                  type: 'text',
                  label: 'Town',
                },
                className: 'col-3',
              },
              {
                key: 'state',
                type: 'input',
                templateOptions: {
                  required: true,
                  type: 'text',
                  label: 'State',
                },
                className: 'col-4',
              },
              {
                key: 'country',
                type: 'input',
                templateOptions: {
                  required: true,
                  type: 'text',
                  label: 'Country',
                },
                className: 'col-4',
              },
              {
                type: 'repeat-delete',
                templateOptions: {
                  label: 'Remove',
                },
                className: 'col-1'
              },

            ],
          },
        },

      ],
    },

    { template: '<hr />' },

  ];

  onSubmit() {
    if (this.form.valid) {
      console.log(this.model)
    } else {
      alert('Form is invalid')
    }
  }


  // Address panel
  showAddressPanel: boolean = false;

  hideShow(event: any) {
    const field = this.fields.find(f => f.key === 'address');
    field!.hide = !field!.hide;

    if (field!.hide) {
      field!.templateOptions!.label = 'Add Address';
      this.form.get('address.town')?.reset();
      this.form.get('address.state')?.reset();
      this.form.get('address.country')?.reset();
    }

    this.showAddressPanel = !this.showAddressPanel;
  }

  hideShowBtn(event: any) {
    this.showAddressPanel = !this.showAddressPanel;
  }
}

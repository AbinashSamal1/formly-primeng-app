import { Component } from '@angular/core';
import { FieldArrayType } from '@ngx-formly/core';

@Component({
  selector: 'formly-repeat-section',
  template: `
    <div *ngFor="let field of field.fieldGroup; let i = index;">
      <formly-field [field]="field"></formly-field>
    </div>
    <div class="mt-2">
      <div>
        <p-button icon="pi pi-plus" severity="info" (click)="add()" [rounded]="true" [text]="true" 
        [raised]="true"></p-button>
      </div>

    </div>
  `,
})
export class RepeatTypeComponent extends FieldArrayType {
  ngOnInit() {
    this.field.templateOptions!['remove'] = this.remove.bind(this);
  }
}
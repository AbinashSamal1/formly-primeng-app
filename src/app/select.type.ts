import { Component } from '@angular/core';
import { FieldType, FieldTypeConfig } from '@ngx-formly/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'multi-radio',
  template: `
    <h3>{{to.label}}</h3>
      <div *ngFor="let option of optionsArray">
        <input type="radio" [formControl]="formControl" [value]="option.value">
        <label for="">{{ option.label }}</label>
      </div>
  `,
})

export class MultiRadioComponent extends FieldType<FieldTypeConfig> {

  optionsArray: any[] = [];


  ngOnInit(): void {
    if (this.to.options instanceof Observable) {
      this.to.options.subscribe((options: any[]) => {
        this.optionsArray = options;
      });
    } else if (Array.isArray(this.to.options)) {
      this.optionsArray = this.to.options;
    }
  }
}

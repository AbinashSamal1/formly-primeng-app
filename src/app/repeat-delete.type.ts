import { Component } from "@angular/core";
import { FieldType } from "@ngx-formly/core";

@Component({
  selector: "formly-field-button",
  template: `
    <div class="flex justify-content-end flex-wrap">
    <div class="flex align-items-center justify-content-center">
      <p-button (click)="remove()" icon="pi pi-times" [rounded]="true" severity="danger" [outlined]="true"></p-button>
    </div>
    </div>
  `
})
export class RepeatDeleteTypeComponent extends FieldType {
  remove() {
    const i = this.field.parent!.key;

    this.field.parent!.parent!.templateOptions!['remove'](i);
  }
}

import { Component } from "@angular/core";
import { FormlyFieldConfig } from "@ngx-formly/core";

@Component({
    selector: 'Required',
    template: ''
})

export class Required {
    static minLengthValidationMessage(error: any, field: FormlyFieldConfig) {
        return `${field.props?.label} Should be at least of ${field.props?.minLength} characters`;
    }
}

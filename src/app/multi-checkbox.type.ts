import { Component } from '@angular/core';
import { FieldType, FieldTypeConfig } from '@ngx-formly/core';
import { Observable } from 'rxjs';

@Component({
    selector: 'formly-field-multi-checkbox',
    template: `
    <div class="card">
    <div>
        <div *ngFor="let option of optionsArray" class="field-checkbox">
            <p-checkbox [label]="option.label" name="group" [value]="option.value" [formControl]="formControl"></p-checkbox>
        </div>
    </div>
`,
})
export class MultiCheckboxTypeComponent extends FieldType<FieldTypeConfig> {

    optionsArray: any[] = [];
    ngOnInit() {
        if (this.to.options instanceof Observable) {
            this.to.options.subscribe((options: any[]) => {
                this.optionsArray = options;
            });
        } else if (Array.isArray(this.to.options)) {
            this.optionsArray = this.to.options;
        }
    }
}
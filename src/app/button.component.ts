import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
    selector: 'app-formly-field-button',
    template: `

  `,
})
export class FormlyFieldButton extends FieldType {
    onClick($event: Event) {
        if (this.props['onClick']) {
            this.props['onClick']($event);
        }
    }
}

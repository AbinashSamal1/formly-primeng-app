import { Component } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
    selector: 'formly-wrapper-panel',
    template: `
    <div class="mt-3 mb-3">
      <p-card class="card" header={{to.label}}>
        <!-- <h3 class="card-header">{{ to.label }}</h3> -->
        <div class="card-body">
          <ng-container #fieldComponent></ng-container>
        </div>
      </p-card>
    </div>
  `,
})
export class PanelWrapperComponent extends FieldWrapper {
}


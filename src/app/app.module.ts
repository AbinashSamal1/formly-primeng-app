import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';
import { CardModule } from 'primeng/card';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormlyFieldConfig, FormlyModule } from '@ngx-formly/core';
import { AbstractControl, FormsModule, ReactiveFormsModule, ValidationErrors, Validators } from '@angular/forms';
import { FormlyPrimeNGModule } from '@ngx-formly/primeng';
import { ButtonModule } from 'primeng/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CheckboxModule } from 'primeng/checkbox'
import { PanelWrapperComponent } from './panel-wrapper.component';
import { FormlyFieldButton } from './button.component';
import { RepeatDeleteTypeComponent } from './repeat-delete.type';
import { RepeatTypeComponent } from './repeat-section.type';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { FormlyDatepickerModule } from '@ngx-formly/primeng/datepicker';
import { MultiRadioComponent } from './select.type';
import { MultiCheckboxTypeComponent } from './multi-checkbox.type';
import { FileUploadModule } from 'primeng/fileupload';
import { FileType } from './wrappers/upload-type/file-type';
// import { FileValueAccessor } from './wrappers/upload-type/file-value-accessor';

@NgModule({
  declarations: [
    AppComponent,
    PanelWrapperComponent,
    FormlyFieldButton,
    RepeatDeleteTypeComponent,
    RepeatTypeComponent,
    MultiRadioComponent,
    MultiCheckboxTypeComponent,
    FileType,
    // FileValueAccessor
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormlyPrimeNGModule,
    CardModule,
    BrowserAnimationsModule,
    CheckboxModule,
    ButtonModule,
    ToastModule,
    FormlyDatepickerModule,
    FileUploadModule,
    FormlyModule.forRoot(
      {
        validationMessages: [
          { name: 'required', message: requireFields },
          { name: 'minLength', message: minLengthValidationMessage },
        ],
        extras: { lazyRender: true },
        types: [
          {
            name: 'repeat-delete',
            component: RepeatDeleteTypeComponent,
            // defaultOptions: {
            //   templateOptions: {
            //     btnType: 'default',
            //     type: 'button',
            //   },
            // },
          },
          { name: 'repeat', component: RepeatTypeComponent },
          { name: 'multi-radio', component: MultiRadioComponent },
          { name: 'multi-checkbox', component: MultiCheckboxTypeComponent, wrappers: ['form-field'] },
          { name: 'file', component: FileType, wrappers: ['form-field'] }
        ],
        wrappers: [
          { name: 'panel', component: PanelWrapperComponent },
        ],
      }
    ),
  ],
  providers: [
    provideClientHydration(),
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

// Required
export function requireFields(error: any, field: FormlyFieldConfig) {
  return `${field.props?.label} is a required field`
}

// Minimum length Validator
export function minLengthValidationMessage(error: any, field: FormlyFieldConfig) {
  return `${field.props?.label} Should be at least of ${field.props?.minLength} characters`;
}

// Email pattern validator
export function emailValidator(control: AbstractControl): ValidationErrors | null {
  return !control.value || /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(control.value) ? null : { email: true };
}